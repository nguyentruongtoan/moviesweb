# MoviesWeb

Xây dựng trang HTML cung cấp thông tin về movies (lấy từ Web API) với các chức năng sau :
- Tìm kiếm phim theo tên, theo diễn viên.
- Cho phép xem chi tiết thông tin phim.
- Cho phép xem thông tin diễn viên.